def configuration() {
    env.rhea_project = "freedom-e-sdk-rhea_old"
    env.semi_project = "soc-walnutcakeverilog-semifive"	
    env.default_branch = "master_ci_test"
    env.test_branch = "${env.BRANCH}"
    env.group = "semifive"
    env.gitlab = "gitlab.semifive.com"
}

def module(){
    wit = "sifive/wit/0.14.1"
    vcs = "synopsys/vcs/P-2019.06-SP2-12"
    verdi = "synopsys/verdi/Q-2020.03-SP2-3"
    riscv = "riscv-tools/10.2.0 "
}

def path(){
    tmp_pwd = sh(script: "pwd", returnStdout:true).trim()
    tmp_basedir = sh(script: "basename ${tmp_pwd}", returnStdout:true).trim()
	environment = "/continuous-integration/semifive-jenkins/environment.sh"
	archivePath = "/user/jenkins/archive"
	modulePath = "/semifive/tools/Modules/default/init/zsh"
    simPath = "TOP/rtl.meta/rtl.sim"
}

def prepareWit(){
    sh """
        source ${modulePath}
        module load ${wit}
        wit init . -a git@${env.gitlab}:${env.group}/${env.rhea_project}.git
    """
}

def compile(){
    sh """
        source ${modulePath}
        cd ${env.rhea_project}
        module load ${riscv}
        git submodule update --init --recursive
        git checkout ${env.test_branch}
        make PROGRAM=hello TARGET=design-rtl CONFIGURATION=release
    """	
}

def runSim(){
    sh """
        source ${modulePath}
        module load ${vcs}
        module load ${verdi}
        cd ${env.semi_project}
        git checkout ${env.default_branch}
        cd ${simPath}
        make clean
        make hello.vcs.out
    """
}

def set_environment(){
	tmp_cause = "unknown cause"
	tmp_username = "unknown username"
	tmp_branch = "master"
	switch (currentBuild.getBuildCauses()._class[0]){
		case 'hudson.model.Cause$UserIdCause':
			tmp_cause =  "manual"
			tmp_username = currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause').userName[0]
			tmp_branch = "master"
			break
		case 'com.dabsquared.gitlabjenkins.cause.GitLabWebHookCause':
			tmp_cause = "gitlab"
			tmp_username = "$gitLabUserName"
			tmp_branch = "$gitlabAfter"
			break
		case 'hudson.triggers.TimerTrigger$TimerTriggerCause':
			tmp_cause =  "periodically"
			tmp_username = "periodically"
			tmp_branch = "master"
			break
		case 'org.jenkinsci.plugins.gwt.GenericCause':
			tmp_cause = "generic webhook (to be updated soon)"
			tmp_username = "generic webhook (to be updated soon)"
			break
		case 'hudson.model.Cause$UpstreamCause':
			tmp_cause = "upstream"
			tmp_username = "upstream"
			tmp_branch = "master"
		default:
			break
	}
	if (tmp_cause != "gitlab"){
		if(tmp_cause == "upstream"){
				currentBuild.setDescription("Build from ${tmp_cause} from ${currentBuild.getBuildCauses().upstreamProject} at ${currentBuild.getBuildCauses().upstreamBuild}")
		} else if(tmp_cause != "periodically"){
			currentBuild.setDescription("Build from ${tmp_username} with ${tmp_cause}")
		} else {
			currentBuild.setDescription("Build ${tmp_cause}")
		}
	}

	x = currentBuild.getDescription()
	currentBuild.setDescription("Node on ${NODE_NAME}\n${x}")

	env.CAUSE = tmp_cause
	env.USER = tmp_username
	env.BRANCH = tmp_branch
}

def archive(){
    try{
        if (fileExists("${archivePath}/${JOB_NAME}") == false){
                sh "mkdir -p ${archivePath}/${JOB_NAME}"
        }	
        sh "tar -czf ${archivePath}/${JOB_NAME}/${JOB_NAME}.${BUILD_NUMBER}.tar.gz --ignore-failed-read --exclude=.fuse ../${tmp_basedir}"
    }catch(e){
        println("archive error")
    }
}   

def checkFailure(logPath){
	tmp_pwd = sh(script: "pwd", returnStdout:true).trim()
	dir("${tmp_pwd}/${env.semi_project}/${simPath}") {
		read = readFile 'sim.out'
		if((read =~ /Assertion Failed/))
   		{
			sh """
				set +x
				exit 1
			"""
		}
	}
}
return this