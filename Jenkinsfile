def AGENT = "uranus"

def configuration() {
    env.rhea_project = "freedom-e-sdk-rhea"
    env.semi_project = "soc-walnutcakeverilog-semifive"	
    env.vcs = "synopsys/vcs/P-2019.06-SP2-12"
    env.verdi = "synopsys/verdi/Q-2020.03-SP2-3"
    env.riscv = "riscv-tools/10.2.0 "
}

def path_configuration(){
	env.environment = "/continuous-integration/semifive-jenkins/environment.sh"
	env.archivePath = "/user/jenkins/archive"
	env.module = "/semifive/tools/Modules/default/init/zsh"
}

// do configuration
def do_configuration = configuration()
def do_path_configuration = path_configuration()

properties properties: [[$class: 'GitLabConnectionProperty', gitLabConnection: 'Portal Gitlab']]
properties properties: [[$class: 'GitLabConnectionProperty', gitLabConnection: 'Venus Gitlab']]
pipeline{
	/*
	*   agent
	*/
	agent {
		label "${AGENT}"
	}    

    stages {
        stage('source'){
            steps{
                updateGitlabCommitStatus(name : "jenkins", state : "running")
                script{
					// clean current directory
                    cleanWs()
					//prepare
                    sh """
                        source ${env.module}
                        module load sifive/wit/0.14.1
                        wit init . -a git@gitlab.semifive.com:semifive/${env.rhea_project}.git
                    """	
                }
            }
        }	
        stage('compile'){
            steps{
                updateGitlabCommitStatus(name : "jenkins", state : "running")
                script{
                    sh """
                        source ${env.module}
                        cd ${env.rhea_project}
                        module load ${env.riscv}
                        git submodule update --init --recursive
                        make PROGRAM=ci TARGET=design-rtl CONFIGURATION=release
                    """	
                }
            }
        }	        	
        stage('make'){
            steps{
                script{
                    sh """
                        source ${env.module}
                        module load ${env.vcs}
                        module load ${env.verdi}
                        cd ${env.semi_project}
                        git checkout step2-top-CI
                        cd TOP/rtl.meta/rtl.sim
                        make clean
                        make ci
                    """
                    check_Failure(env.logPath)
                }
            }
        }        
    }
	post {
		/*
			success
			when success
		*/
		always {
			script{
                try{
                    archivePath = "/user/jenkins/archive"
                    if (fileExists("${archivePath}/${JOB_NAME}") == false){
                            sh "mkdir -p ${archivePath}/${JOB_NAME}"
                        }	
                        tmp_pwd = sh(script: "pwd", returnStdout:true).trim()
                        tmp_basedir = sh(script: "basename ${tmp_pwd}", returnStdout:true).trim()
                        sh "tar -czf ${archivePath}/${JOB_NAME}/${JOB_NAME}.${BUILD_NUMBER}.tar.gz --ignore-failed-read --exclude=.fuse ../${tmp_basedir}"
                }catch(e){
                    println("archive error")
                }
			}
		}
		success {
			updateGitlabCommitStatus(name : "jenkins", state : "success")
			script{
			    println("success")
			}
		}
		failure {
			updateGitlabCommitStatus(name : "jenkins", state : "failed")
			script{
			    println("failure")
			}
		}
		aborted {
			updateGitlabCommitStatus(name : "jenkins", state : "canceled")
			script{
			    println("aborted")
            }
		}
	}
}

def check_Failure(logPath){
	tmp_pwd = sh(script: "pwd", returnStdout:true).trim()
	dir("${tmp_pwd}/${env.semi_project}/TOP/rtl.meta/rtl.sim") {
		read = readFile 'sim.out'
		if((read =~ /Assertion Failed/))
   		{
			sh """
				set +x
				exit 1
			"""
		}
	}
}
